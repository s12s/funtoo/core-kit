# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit bash-completion-r1 go-module

go-module_set_globals

DESCRIPTION="Docker CLI plugin for extended build capabilities with BuildKit"
HOMEPAGE="https://github.com/docker/buildx"
SRC_URI="https://github.com/docker/buildx/tarball/1360a9e8d25a2c3d03c2776d53ae62e6ff0a843d -> buildx-0.21.2-1360a9e.tar.gz
https://fun.s12s.xyz/distfiles/docker-buildx-0.21.2-funtoo-go-bundle-2c126f5f6427e0792c349477ee802cdfe8f0e34fcefa7955a9629e85af7a620ef1fc095fe988e9cab1222a1460033891e005db9dedc6e99e4497b9c440f72ff9.tar.gz -> docker-buildx-0.21.2-funtoo-go-bundle-2c126f5f6427e0792c349477ee802cdfe8f0e34fcefa7955a9629e85af7a620ef1fc095fe988e9cab1222a1460033891e005db9dedc6e99e4497b9c440f72ff9.tar.gz"

LICENSE="Apache-2.0"
SLOT="2"
KEYWORDS="*"

RDEPEND=">=app-emulation/docker-cli-23.0.0"

RESTRICT="test"

post_src_unpack() {
	if [ ! -d "${S}" ]; then
		mv docker-buildx* "${S}" || die
	fi
}

src_prepare() {
	default
	# do not strip
	sed -i -e 's/-s -w//' Makefile || die
}

src_compile() {
	export CGO_CPPFLAGS="${CPPFLAGS}"
	export CGO_CFLAGS="${CFLAGS}"
	export CGO_CXXFLAGS="${CXXFLAGS}"
	export CGO_LDFLAGS="${LDFLAGS}"
	export GOFLAGS='-buildmode=pie -trimpath -mod=readonly -modcacherw'
	export GO111MODULE=on
	_buildx_r=github.com/docker/buildx
	go build -mod=vendor -o docker-buildx -ldflags "-linkmode=external \
		-compressdwarf=false \
		-X $_buildx_r/version.Version=0.21.2
		-X $_buildx_r/version.Revision=1360a9e8d25a2c3d03c2776d53ae62e6ff0a843d
		-X $_buildx_r/version.Package=$_buildx_r" \
		./cmd/buildx
}

src_test() {
	emake test
}

src_install() {
	exeinto /usr/libexec/docker/cli-plugins
	doexe docker-buildx
	dodoc README.md
}